GameMessages = require './game_io'
_ = require 'underscore'
message = require './message'

class Betting
	# @betTimeout is in seconds
	constructor: (@id, @io, @betTimeout) ->
		console.log "Started betting class with a timeout of " + @betTimeout + " seconds"

	placeBet: (player, options) ->

		onBet = (data) =>
			# ignore actions that the player isn't allowed to take
			if _.indexOf(options.actions, data.action) is -1
				console.log "\tPlayer #{player.name} took invalid action, ignoring it"
				return

			console.log "\tPlayer took action, running callback"
			options.callback player, data.action, data.chipsBet

		onTimeout = ->
			console.log "Betting.timeoutCallback for " + player.name
			options.callback player, GameMessages.FOLD, 0

		params =
			position: options.position
			wager: options.wager
			minBet: options.minBet
			minRaise: options.minRaise
			maxRaise: options.maxRaise
			betIncrement: options.increment
			chipsAvailable: player.chips
			allowedActions: options.actions
			timeout: @betTimeout

		response =
			io: player.user
			message: GameMessages.BET
			onMessage: _.bind onBet, @
			onDisconnect: _.bind onTimeout, @
			onTimeout: _.bind onTimeout, @
			timeout: @betTimeout
			singleShot: true
			requiredParams:
				['action', 'chipsBet']

		@cancelCall =
			message.send @io.sockets.in(@id), GameMessages.BET, params, response

	cancelBet: ->
		@cancelCall()

exports = module.exports = Betting
