game = require './game'
hand_lib = require './hand'
Seating = require './seating'
PlayerInfo = require './player_info'
Player = require './player'
GameMessages = require './game_io'
models = require '../models/models'
message = require './message'
handevaluator = require './evaluator'

_ = require 'underscore'

class Table
	DEFAULT_TABLE_SIZE = 10
	HEADSUP_TABLE_SIZE = 2
	DEFAULT_GAME_START_DELAY = 1

	constructor: (@id, @io, type) ->
		@seating = new Seating DEFAULT_TABLE_SIZE
		@dealerPosition = -1
		@smallBlind = -1
		@bigBlind = -1
		switch type
			when '1' then @baseBet = 5
			when '2' then @baseBet = 25
			when '3' then @baseBet = 50
			else @baseBet = 5

		# TODO move to a "game rules" class of its own
		@minBuyIn = @baseBet * 2
		@maxBuyIn = @baseBet * 20

		@isGameStarted = false

	seatUser: (user, position) ->
		# when user sits down, they become a player
		# however, we just reserve their seat and wait for them to buy in
		console.log 'Table.seatUser'

		player = new Player user

		if player.user.doc.chips < @minBuyIn
			console.log player.name + " didn't have enough chips to buy in"
			player.user.emit GameMessages.NO_BUYIN
			return false

		success = @seating.seat player, position

		if success

			message.listen
				io: player.user
				message: GameMessages.TABLE_GETUP
				singleShot: true
				onMessage: _.bind @onPlayerGetUp, @
				onDisconnect: _.bind @onPlayerDisconnect, @
				additionalParams:
					player: player

			player.state = game.PlayerStates.OBSERVING

			@sendBuyinToPlayer player

		return success

	sendBuyinToPlayer: (player) ->
		# don't send players who already have a buy-in another one
		if player.clearBuyinListen
			return

		player.clearBuyinListen = 
			message.listen
				io: player.user
				message: GameMessages.BUYIN
				onMessage: _.bind @onPlayerBuyin, @
				requiredParams: ['chips']
				additionalParams:
					player: player

		player.user.emit GameMessages.BUYIN,
			min: @minBuyIn,
			max: Math.min(player.user.doc.chips, @maxBuyIn),
			available: player.user.doc.chips

	onPlayerDisconnect: (user, additionalParams) ->
		# kinda hacky, but better than duplicating the functionality of
		# onPlayerGetUp in here
		@onPlayerGetUp user, null, additionalParams

	onPlayerGetUp: (user, clearListeners, additionalParams) ->
		player = additionalParams.player
		console.log "#{player.name} left the table" 
		position = @seating.find player
		@seating.remove position

		# if server was listening for player to buy in, clear it
		if player.clearBuyinListen?
			player.clearBuyinListen()
			delete player.clearBuyinListen

		# return player's chips for this game back to their main chip pile
		player.cashOutChips()

		@io.sockets.in(@id).emit GameMessages.TABLE_GETUP, 
			name: player.name, position: position

		activePlayers = 0

		@seating.forEachFrom 1, (player) ->
			activePlayers++ if player.state is game.PlayerStates.PLAYING

		console.log "\t Players left: " + activePlayers			

		if activePlayers < 2 and @game?
			console.log 'Table.onPlayerGetUp: one player left, ending game!'
			@game.nextState = game.GameStates.SHOWDOWN
			@game.cancelBetting()
			@game.advanceState()

	onPlayerBuyin: (data, user, clearListeners, additionalParams) ->
		player = additionalParams.player

		if not @isValidBuyin player.user.chips, data.chips
			console.log 'User tried to buy in with invalid number of chips, ignoring'
			return

		player.buyInChips data.chips

		player.state = game.PlayerStates.BOUGHT_IN

		# don't listen to buy-in messages from this player anymore
		clearListeners()

		if player.clearBuyinListen?
			delete player.clearBuyinListen

		# update rest of players in room
		@io.sockets.in(@id).emit GameMessages.PLAYER_BOUGHT_IN,
			position: @seating.find player
			chips: player.chips

		console.log "#{player.name} (#{player.state}) bought in with #{player.chips} chips"
		@tryToStartGame()

	startNewGame: ->
		# check to see if anyone left between when we wanted to start 
		# game and when actually started it
		count = 0
		@seating.forEachFrom 1, (player) ->
			if player.state is game.PlayerStates.BOUGHT_IN or
			player.state is game.PlayerStates.PLAYING
				++count

		if count < 2
			console.log 'Not enough active players to start the game.'
			@isGameStarted = false
			return

		console.log "starting new game"

		# set dealer position
		if @dealerPosition is -1
			@dealerPosition = @seating.first()
		else
			# advance dealer position
			@dealerPosition = @seating.nextActive @dealerPosition

		# set small blind position
		@smallBlind = @seating.nextActive @dealerPosition
		# set big blind position
		@bigBlind = @seating.nextActive @smallBlind

		@game = new game.Game @id, @io, @seating, @dealerPosition, @baseBet, 
			@smallBlind, @bigBlind, 
			_.bind(@startOfGame, @), 
			_.bind(@endOfGame, @),
			handevaluator.get()

		console.log "Made a game object"

	tryToStartGame: (afterWaitingSeconds = DEFAULT_GAME_START_DELAY) ->
		console.log 'tryToStartGame'
		# check if table has enough active players (2)
		count = 0
		@seating.forEachFrom 1, (player) ->
			if player.state is game.PlayerStates.BOUGHT_IN or 
			player.state is game.PlayerStates.PLAYING
				++count					

		if count < 2
			console.log 'Not enough active players to start the game.'
			return

		if @isGameStarted
			console.log 'Game already in progress, not starting new game'
			return

		@isGameStarted = true
		console.log '\t\t\t\tisGameStarted == ' + @isGameStarted

		@resetPlayers()

		activePlayers = 0

		@seating.forEachFrom 1, (player) =>
			if player.state is game.PlayerStates.BOUGHT_IN
				player.state = game.PlayerStates.PLAYING
				activePlayers++

		console.log "Starting game with #{activePlayers} players"

		@io.sockets.in(@id).emit GameMessages.START, afterWaitingSeconds
		setTimeout _.bind(@startNewGame, @), afterWaitingSeconds * 1000

	resetPlayers: ->
		@seating.forEachFrom 1, (player) =>
			player.wager = 0
			player.hand = []
			player.state = game.PlayerStates.BOUGHT_IN

			console.log "#{player.name} has #{player.chips} chips"

			if player.chips is 0
				player.state = game.PlayerStates.OBSERVING

				if player.user.doc.chips > @minBuyIn
					@sendBuyinToPlayer(player)
				else
					console.log player.name + " didn't have enough chips to buy in"
					player.user.emit GameMessages.NO_BUYIN

	startOfGame: ->
		console.log "startOfGame"
		@game.advanceState()

	endOfGame: ->
		# reset wagers and hands
		@resetPlayers()

		@game = undefined
		@isGameStarted = false

		console.log 'endOfGame'
		console.log '\t\t\tisGameStarted == ' + @isGameStarted

		# start new game
		@tryToStartGame()

	# build a Backbone model of the table seating
	getSeatingModel: ->
		console.log 'getSeatingModel'
		model = new models.PlayerSeatingModel
		#model.clear()
		@seating.forEachFrom 1, (user, position) ->
			console.log 'seating ' + user.name + ' at ' + position
			console.log "typeof seat == " + typeof position
			model.seatPlayer user.name, position

		console.log "model length == #{model.players.size()}"

		return model

	# this is fired when a user joins the room that the table is a part of
	onUserJoin: (user) ->
		# send the user a complete model of the table
		@seating.forEachFrom 1, (player, position) ->
			user.emit GameMessages.TABLE_SIT, 
				name: player.name
				position: position
				chips: player.chips

		# if we have an active game, delegate user join event to game
		if @game?
			@game.onUserJoin user

	isValidBuyin: (userChips, buyin) ->
		buyin <= userChips and (@minBuyIn <= buyin <= @maxBuyIn)


module.exports = Table
