# this class defines the protocol for sending and receiving game-related messages

GameMessages =
	ERROR: 'error'

	# room-specific messages
	ROOM_JOIN: 'join'
	TABLE_SIT: 'sit'
	TABLE_SIT_RESPONSE: 'sitResponse'
	TABLE_GETUP: 'getup'
	ROOM_LEAVE: 'leave'
	ROOM_MSG: 'roomMessage'
	PLAYER_SEATING: 'playerSeating'
	BUYIN: 'buyin'
	NO_BUYIN: 'noBuyin'

	# game turn messages
	GAME_INFO: 'gameInfo'
	PLAYER_INFO: 'playerInfo'
	PLAYER_BOUGHT_IN: 'playerBoughtIn'
	START: 'start'
	BETTING: 'betting'
	PREFLOP: 'preflop'
	FLOP: 'flop'
	TURN: 'turn'
	RIVER: 'river'
	SHOWDOWN: 'showdown'
	CARDS: 'cards'
	GAME_END: 'gameEnd'

	# player betting messages
	BET: 'bet'
	CHECK: 'check'
	CALL: 'call'
	RAISE: 'raise'
	FOLD: 'fold'
	ALL_IN: 'allIn'
	BIG_BLIND: 'bigBlind'
	SMALL_BLIND: 'smallBlind'

`
if (typeof exports !== 'undefined') {
		exports = module.exports = GameMessages;
} else {
		this.GameMessages = GameMessages;
}`
