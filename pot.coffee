_ = require 'underscore'

class Pot
	constructor: ->
		@bets = {}

	setBet: (name, chips) ->
		@bets[name] = chips

	getBet: (name) ->
		if not @bets[name] then return 0
		else return @bets[name]

	totalValue: ->
		values = _.values @bets
		if _.isEmpty values then return 0
		else return _.values(@bets).reduce (accumulator, value) -> 
			accumulator + value

module.exports = Pot