_ = require 'underscore'
models = require '../models/models'

class Card
	Card.SUITS = "CDHS"
	Card.FACES = "23456789TJQKA"

	constructor: (@symbol) ->
		@suit = @symbol[1]
		@face = @symbol[0]
		@suit.toUpperCase()

	toString: ->
		[@face, @suit].join ''

	toModel: ->
		model = new models.CardModel 
			symbol: @suit.toLowerCase() + @face.toLowerCase()

		return model

class Deck
	constructor: ->
		# build a full deck
		@cards = []
		@drawnCards = []

		for face in Card.FACES
			for suit in Card.SUITS
				@cards.push new Card face + suit

	shuffle: ->
		# Fisher-Yates shuffle (or so underscore claims)
		@cards = _.shuffle @cards

	# returns the specified number of card from the front of the deck
	# and adds it to the list of drawn cards
	drawCard: (numCards = 1) ->
		cards = @cards.splice 0, numCards
		@drawnCards.unshift cards.reverse()
		_.flatten @drawnCards
		return cards


class Hand
	# TODO [important]: make sure that there is option to prevent duplicates
	constructor: (@cards) ->
	
	rank: ->
		if @.royal_flush()
			return "Royal Flush"
		else if @.four_of_a_kind()
			return "Four of a Kind"
		else if @.full_house()
			return "Full house"
	
	sort_by_suit: (reverse=false) ->
		cards = _.sortBy @cards, (card) ->
			card.suit
		cards.reverse() if reverse

	sort_by_face: ->
		cards = _.sortBy @cards, (card) ->
			card.face
		cards.reverse()
	
	face_values: ->
		_.map @cards, (card) ->
			card.face
	# Sorts hand by face and transforms it to a string
	to_string: ->
		strings = []
		cards = @.sort_by_face()
		cards.forEach (card, i) ->
			strings.push(card.symbol)
		strings.join(" ")
	
	royal_flush: ->
		regex = /A(.) K\1 Q\1 J\1 T\1/
		regex.test(@.to_string())

	four_of_a_kind: ->
		regex = /(.). \1. \1. \1./
		regex.test(@.to_string())
	
	full_house: ->
		regex1 = /(.). \1. \1. (.*)(.). \3./
		regex2 = /((.). \2.) (.*)((.). \5. \5.)/
		if regex1.test(@.to_string())
			true
		else if regex2.test(@.to_string())
			true
		else
			false
	
	flush: ->
		regex = /(.)(.) (.)\2 (.)\2 (.)\2 (.)\2/
		regex.test(@.to_string())
	
	three_of_a_kind: ->
		regex = /(.). \1. \1./
		regex.test(@.to_string())

	two_pair: ->
		regex = /(.). \1.(.*?) (.). \3./
		regex.test(@.to_string())
	
	pair: ->
		regex = /(.). \1./
		regex.test(@.to_string())
	
	highest_card: ->
		cards = @.sort_by_face()
		cards[0]
# Examples
#hand = new Hand "Jh Th Qh Ah Kh" # Royal Flush
#hand2 = new Hand "Kh Ks 3h As 3s"
#hand3 = new Hand "Kh 3h Ks"

exports = module.exports

exports.Card = Card
exports.Deck = Deck
exports.Hand = Hand
