#define BUILDING_NODE_EXTENSION

#include "handevaluator.h"
#include <sys/stat.h>
#include <stdio.h>
#include "stdlib.h"
#include <string.h>
#include <node.h>
#include <v8.h>
#include <uv.h>
#include <unistd.h>

using namespace v8;

#define REQ_FUN_ARG(I, VAR)                                             \
  if (args.Length() <= (I) || !args[I]->IsFunction())                   \
    return ThrowException(Exception::TypeError(                         \
                  String::New("Argument " #I " must be a function")));  \
  Local<Function> VAR = Local<Function>::Cast(args[I]);

class HandEval;

struct HandEvalData
{
    HandEval* hand_eval;
    Persistent<Function> callback;
};

void handeval_init(HandEvalData* data);
handeval_eq_class* calculate_equivalence_class(char* hand, HandEval* data);

class HandEval : public node::ObjectWrap
{
public:
    static void Init(Handle<Object> exports)
    {
        // printf("HandEval::Init\n");
        // fflush(stdout);

        Local<FunctionTemplate> tpl = FunctionTemplate::New(New);
        tpl->SetClassName(String::NewSymbol("HandEval"));
        tpl->InstanceTemplate()->SetInternalFieldCount(1);

        tpl->PrototypeTemplate()->Set(String::NewSymbol("Load"),
            FunctionTemplate::New(Load)->GetFunction());

        tpl->PrototypeTemplate()->Set(String::NewSymbol("Eval"),
            FunctionTemplate::New(Eval)->GetFunction());

        Persistent<Function> constructor = Persistent<Function>::New(tpl->GetFunction());
        exports->Set(String::NewSymbol("HandEval"), constructor);
    }

    static Handle<Value> New(const Arguments& args)
    {
        // printf("HandEval::New\n");
        // fflush(stdout);
        HandEval* hand_eval = new HandEval;
        hand_eval->Wrap(args.This());
        return args.This();
    }

    static Handle<Value> Load(const Arguments& args)
    {
        // printf("HandEval::Load\n");
        // fflush(stdout);
        HandleScope scope;

        REQ_FUN_ARG(0, callback);

        HandEval* hand_eval = ObjectWrap::Unwrap<HandEval>(args.This());

        HandEvalData* data = new HandEvalData;
        data->hand_eval = hand_eval;
        data->callback = Persistent<Function>::New(callback);

        hand_eval->Ref();

        uv_async_t* async = new uv_async_t;
        async->close_cb = UV_AfterLoad;
        async->data = data;

        uv_async_init(uv_default_loop(), async, UV_Load);
        uv_async_send(async);

        return Undefined();
    }

    static void UV_Load(uv_async_t* handle, int status)
    {
        handeval_init(static_cast<HandEvalData*>(handle->data));
        uv_close((uv_handle_t*)handle, handle->close_cb);
    }

    static void UV_AfterLoad(uv_handle_t* handle)
    {
        HandleScope scope;
        HandEvalData* data = static_cast<HandEvalData*>(handle->data);
        data->hand_eval->Unref();

        data->callback->Call(Context::GetCurrent()->Global(), 0, NULL);

        data->callback.Dispose();

        delete data;
    }

    static const char* ToCString(const v8::String::Utf8Value& value)
    {
        return value.length() != 0 ? *value : "<string conversion failed>";
    }

    static Handle<Value> Eval(const Arguments& args)
    {
        HandleScope scope;

        HandEval* hand_eval = ObjectWrap::Unwrap<HandEval>(args.This());

        String::Utf8Value str(args[0]);
        char* cp = (char*)ToCString(str);
        char* cphand = str_to_cards(cp, 7);
        handeval_eq_class* res = calculate_equivalence_class(cphand, hand_eval);
        int rank = (int)res->id;

        // return value of str_to_cards is dynamically allocated and needs to be freed
        free(cphand);

        Local<Number> num = Number::New(rank);
        return scope.Close(num);
    }

    // static Persistent<FunctionTemplate> s_ct;

    handeval_dag_node *dag;
    handeval_eq_class *classes[7462];
};

// Persistent<FunctionTemplate> HandEval::s_ct;

void error(char* msg)
{
	fprintf(stderr, "ERROR: %s\n\n", msg);
	exit(-1);
}

void errorline(char* msg, unsigned int line)
{
	fprintf(stderr, "ERROR in line %u: %s\n\n", line, msg);
	exit(-1);
}

void load_equivalenceclasses(char* fn, HandEvalData* data)
{
    handeval_eq_class** classes = data->hand_eval->classes;

	char buf[1024];
	static const char *DELIM = "\t";
	unsigned int i=1,j;

    FILE* f = fopen(fn, "r");

    while (fgets(buf, 1024, f))
    {
        // read a line
        // 1	AKQJT	8	Royal Flush	100.00	0.0032
        classes[i] = (handeval_eq_class*) malloc(sizeof(handeval_eq_class));
        classes[i]->id = i;
        strtok(buf, DELIM);
        classes[i]->cards = strdup(strtok(0, DELIM));
        classes[i]->type = atoi(strtok(0, DELIM));
        classes[i]->desc = strdup(strtok(0,DELIM));
        classes[i]->domination = atof(strtok(0,DELIM));
        classes[i]->likelihood = atof(strtok(0,DELIM));
        i++;
    }

    fclose(f);
}

void load_dag(char* fn, HandEvalData* data)
{
	static char DELIM[2] = {'\t', '\n'};
	char buf[1024];
	unsigned int i, id, v, j;
	int ecf, ecn;
	handeval_dag_node *n;
    data->hand_eval->dag = (handeval_dag_node*) calloc(76154, sizeof(handeval_dag_node));
	
	FILE* f = fopen(fn, "r");
	i=0;
	while (fgets(buf, 1024, f)) {	// read a line
		if ((id = atoi(strtok(buf, DELIM))) != i) {
			errorline("Nonmatching node ID!", i+1);
		}
        n = &(data->hand_eval->dag[i]);	// get the target node
		n->id = id;		// set the id
		for (j=0; j < 13; j++) {	// iterate the target cards
			v = atoi(strtok(0, DELIM));
			if (v > 0) {	// if valid link - invalid links remain set to 0 (by calloc)
                n->succs[j] = &(data->hand_eval->dag[v]);	// set card target
			}
		}
		ecf = atoi(strtok(0, DELIM));	// get and set the equivalence classes
		ecn = atoi(strtok(0, DELIM));
		if (ecf == 0 || ecn == 0 || ecf > 7500 || ecn > 7500) {
			errorline("Invalid equivalence class ID", i+1);			
		}
		n->equiv_class_flush = ecf;
		n->equiv_class_normal = ecn;
		strtok(0, DELIM);					// skip the human-readable part
		n->maxn = atoi(strtok(0, DELIM));
		n->maxf = atoi(strtok(0, DELIM));
		n->minn = atoi(strtok(0, DELIM));
		n->minf = atoi(strtok(0, DELIM));
		i++;
	}
	fclose(f);
}	

handeval_eq_class* calculate_equivalence_class(char* hand, HandEval* data)
{
	// first, walk the dag for non-flush-hands, and count the colors
	unsigned int i, colarr = 0, r, c, flush;			// color array stored in a 32-bit value. I hope that there is some way to do bitfiddeling for flush detection
	char* carr = (char*) &colarr;
	handeval_dag_node* n = 0;
	for (i=0; i < 7; i++) {
		r = hand[i] >> 2;	
		c = hand[i] & 3;
		if (!i) {
            n = &(data->dag[r]);  // the first layer - get the r-th node of the dag
		} else {			
			n = n->succs[r]; // later layers - follow the links (we assume the hand is valid and crash upon errors here)
		}
		carr[c]++;		// count the color
	}
	// next, check for a flush
	flush = 4;
	for (i=0; i < 4; i++) {
		if (carr[i] >= 5) {
			flush = i;
		}
	}
	if (flush == 4) {	// no flush
        return data->classes[n->equiv_class_normal];
	} else {
		// finally, if there is a flush, walk the dag again, and use only the cards of the flush-color
		n = 0;
		for (i=0; i < 7; i++) {	
			r = hand[i] >> 2;	
			c = hand[i] & 3;
			if (c == flush) {	// flush-coloured card?
				if (!n) {
                    n = &(data->dag[r]);  // the first layer - get the r-th node of the dag
				} else {			
					n = n->succs[r]; // later layers - follow the links (we assume the hand is valid and crash upon errors here)
				}
			}
		}
        return data->classes[n->equiv_class_flush];
	}
}

char* hand_to_str(char* hand, unsigned int cards)
{
	unsigned int i;
	char* res = (char*)malloc(cards*3);
	for (i=0; i < cards; i++) {
		res[i*3+0] = CARD[hand[i] >> 2];
		res[i*3+1] = COLOR[hand[i] & 3];
		res[i*3+2] = ' ';
	}
	res[cards*3-1] = 0;
	return res;
}

/** Convert a string to a hand, for debugging purposes. VERY UNSAFE! */
char* str_to_cards(char* str, unsigned int number)
{
	unsigned int i, pos = 0, j;
	char val, r, c;
    char* res = (char*)calloc(number, sizeof(char));
	for (i=0; i < number; i++) {
		r = str[pos++];
		c = str[pos++];
		pos++;
		val = 0;
		for (j=0; j < 13; j++) {
			if (r == CARD[j]) {
				val = j << 2;
			}
		}
		for (j=0; j < 4; j++) {
			if (c == COLOR[j]) {
				val += j;
			}
		}	
		res[i] = val;
	}
	return res;
}

void handeval_init(HandEvalData* data)
{
    // printf("handeval_init\n");
    // fflush(stdout);

    char path[200] = {0};
    // printf("%s\n", getcwd(path, 200));
    load_equivalenceclasses("lib/eqcllist", data);

    // printf("load_equivalenceclasses\n");
    // fflush(stdout);

    load_dag("lib/carddag", data);

    printf("load_dag\n");
    fflush(stdout);

}

void Init(Handle<Object> exports)
{
    HandEval::Init(exports);
}

NODE_MODULE(handevaluator, Init)