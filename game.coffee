Player = require './player'
card_lib = require './hand'
Betting = require './betting'
GameMessages = require './game_io'
models = require '../models/models'
_ = require 'underscore'
Pot = require './pot'

exports = module.exports

PlayerStates =
	OBSERVING: 'observing'
	BOUGHT_IN: 'boughtIn'
	FOLDED: 'folded'
	PLAYING: 'playing'
	ALL_IN: 'allIn'

GameStates =
	START: 'start'	# before cards dealt, blinds need to be posted
	DRAW: 'draw' 	# cards are dealt, go thru and see who calls/folds
	FLOP: 'flop'	# 3 community cards dealt, round of betting ensues
	TURN: 'turn'	# 4 community cards dealt, round of betting ensues
	RIVER: 'river'  # 5 community cards dealt, round of betting ensues
	SHOWDOWN: 'showdown' # everyone shows their cards, determine winner
						 #, handle side pot(s) if necessary

class Game
	BET_TIMEOUT_SECONDS: 300
	POST_GAME_TIME: 10
	MAX_RAISES_PER_STATE: 3

	constructor: (@id, @io, playerSeating, @dealer, @baseBet, @smallBlind, @bigBlind, @startGameCallback, @endGameCallback, @handevaluator) ->
		@nextState = GameStates.START
		@seating = playerSeating
		@gameRaiseCount = 0
		@pots = []
		@community = []
		@deck = new card_lib.Deck
		@deck.shuffle()
		@bet = new Betting @id, @io, Game::BET_TIMEOUT_SECONDS
		@smallBlindBet = Math.floor (@baseBet/2) #small blind bet
		@minBet = @baseBet
		@minRaise = @baseBet*2
		@latestRaise = 0
		@currentPosition = -1
		@doingShowdown = false
		# game types: 1 => LIMIT (ish), 2 => NO LIMIT (ish)
		@game_type = 2

		console.log "constructed game object"

		process.nextTick @startGameCallback

	isGameHeadsUp: ->
		return @seating.count() == 2

	getMaxRaise: (type, player) ->
		if @game_type == 1
			if type is GameMessages.SMALL_BLIND
				return Math.floor (@baseBet / 2)
			else if type is GameMessages.BIG_BLIND
				return @baseBet
			else
				if @nextState is GameStates.DRAW or @nextState is GameStates.FLOP or
				@nextState is GameMessages.TURN
					return @baseBet
				else
					return @baseBet * 2
		# if not limit (whatever)
		else
			# No max raise
			return player.chips

	getMinRaise: (type) ->
		if @game_type == 1
			return
		else
			if type is GameMessages.SMALL_BLIND
				return @smallBlindBet
			else if type is GameMessages.BIG_BLIND
				return @baseBet
			# This step in necessary to make sure that the RE-RAISE minimal raise exists; we define every subsequent raise on that re-raise in the else statement below this one
			else if (@latestRaise == @minRaise || @latestRaise > @minRaise)
				@latestRaise*2
			else
				@minRaise


	getAllowedActions: (player, type) ->
		actions = []
		# possible actions: check, call, raise, fold, all in

		# Allow other actions ONLY if player didn't go all in already
		if player.allIn != true
			# always allow folding
			actions.push GameMessages.FOLD

			# if player has just enough or less than enough chips for the minimum bet, they can go all-in
			if player.chips <= (@minBet - player.wager)
				actions.push GameMessages.ALL_IN

			if type is GameMessages.SMALL_BLIND and player.chips >= @smallBlindBet
				actions.push GameMessages.SMALL_BLIND
				return actions
			#else if type is GameMessages.BIG_BLIND and player.chips >= @baseBet
			else if type is GameMessages.BIG_BLIND and player.chips != 0
				actions.push GameMessages.BIG_BLIND
				return actions

			# if a player's wager is equal to the current bet, they can just check, or not raise the minimum bet at all
			if player.wager == @minBet
				actions.push GameMessages.CHECK

			# Indeed, allow them to all in at any time...
			if player.chips < @minBet
				actions.push GameMessages.ALL_IN

			# if the player has more chips than the minimum bet...
			#if player.chips > (@minBet - player.wager)
			if player.chips > @minBet
				console.log("This player is allowed to RAISE because minBet is #{@minBet}")
				console.log("This player is allowed to RAISE because minRaise is #{@minRaise}")
				console.log("Raise is possible: #{@raisePossible()}")
				# if they have less chips than the minimum amount that they have to raise by, they can still raise by going all-in
				if (player.chips <= (@minBet - player.wager + @baseBet)) and (@gameRaiseCount < Game::MAX_RAISES_PER_STATE) and (player.chips != 0)
					actions.push GameMessages.ALL_IN
				# if they have >= chips than the minimum raise amount, it's just a normal raise
				else if @gameRaiseCount < Game::MAX_RAISES_PER_STATE && @raisePossible()
					console.log("pushed RAISE to a player with: ")
					console.log("\t chips: #{player.chips}")
					console.log("\t wager: #{player.wager}")
					console.log("\t minBet: #{@minBet}")
					console.log("\t minRaise: #{@minRaise}")
					console.log("\t latestRaise: #{@latestRaise}")
					console.log("\t raiseCount: #{@gameRaiseCount}")
					actions.push GameMessages.RAISE

			# if the min bet is higher than the player's wager but the player has enough chips left, they can call
			if player.wager < @minBet and player.chips > (@minBet - player.wager)
				actions.push GameMessages.CALL

		return actions

	advanceState: ->
		console.log 'Advancing to ' + @nextState
		@gameRaiseCount = 0
		@seating.forEachFrom @smallBlind, (player) =>
			player.raiseCount = 0
			console.log "Player wager: #{player.wager}"
			player.allIn = false if @nextState == GameStates.SHOWDOWN

		doNextState = =>
			switch @nextState
				when GameStates.START 		then @postBlinds()
				when GameStates.DRAW 		then @drawHands()
				when GameStates.FLOP 		then @drawFlop()
				when GameStates.TURN 		then @drawTurn()
				when GameStates.RIVER 		then @drawRiver()
				when GameStates.SHOWDOWN	then @showdown()

		setTimeout doNextState, 1000

	postBlinds: ->
		# post blind bets before drawing hands
		# players must be prompted to post blinds or sit out of the current round

		# create main pot
		@pots.push new Pot

		@nextState = GameStates.DRAW

		@currentPosition = @smallBlind
		@bettingPlayer = @seating.at @currentPosition

		console.log 'current position == ' + @currentPosition

		@io.sockets.in(@id).emit GameMessages.GAME_INFO,
			mainPot: @pots[0].totalValue()
			dealer: @dealer

		@bet.placeBet @bettingPlayer,
				position: @currentPosition
				wager: @bettingPlayer.wager
				minBet: @smallBlindBet
				minRaise: @getMinRaise(GameMessages.BET)
				maxRaise: @getMaxRaise(GameMessages.SMALL_BLIND, @bettingPlayer)
				increment: @baseBet
				actions: @getAllowedActions(@bettingPlayer, GameMessages.SMALL_BLIND)
				callback: _.bind @onBet, @

	drawHands: ->
		console.log 'drawHands'

		@nextState = GameStates.FLOP

		# start dealing from small blind and deal out 2 cards to each player
		@seating.forEachFrom @smallBlind, (player, position) =>
			if not @isActive player
				return

			player.hand = @deck.drawCard(2)
			console.log player.hand[0].toModel().toJSON()
			console.log player.hand[1].toModel().toJSON()

			player.user.emit GameMessages.CARDS,
				card1: player.hand[0].toModel().toJSON()
				card2: player.hand[1].toModel().toJSON()
				position: position

		@betting()

	sendCards: (message, to) ->
		target = null
		if to?
			target = to
		else
			target = @io.sockets.in(@id)

		list = []

		for card in @community
			list.push card.toModel()

		target.emit message, cards: list

	drawFlop: ->
		@nextState = GameStates.TURN

		# draw 3 community cards
		@community.push @deck.drawCard(3)
		@community = _.flatten @community

		@sendCards GameMessages.FLOP

		console.log 'flop:'
		console.log (card.toString() for card in @community).join ' '

		@betting()

	drawTurn: ->
		@nextState = GameStates.RIVER

		# draw 4th community card
		@community.push @deck.drawCard()
		@community = _.flatten @community

		@sendCards GameMessages.TURN

		console.info 'turn:'
		console.info (card.toString() for card in @community).join ' '

		@betting()

	drawRiver: ->
		@nextState = GameStates.SHOWDOWN

		# draw 5th/last community card
		@community.push @deck.drawCard()
		@community = _.flatten @community

		@sendCards GameMessages.RIVER

		@betting()

	showdown: ->
		if @doingShowdown
			console.log 'Already started showdown from elsewhere'
			return

		@doingShowdown = true

		# evaluate the hands to determine the winner
		console.log 'These are the community cards:'
		hand = (card.toString() for card in @community).join ' '
		console.log hand

		console.log 'There are ' + @pots.length + ' pots'
		console.log @pots

		# for each pot, determine the winners and then give those people the winnings
		for pot, i in @pots

			# new winners and losers for each pot
			winners = []
			losers = []

			if @nextState is GameStates.DRAW or @nextState is GameStates.FLOP or
			_.isEmpty @community
				# if we're before community card dealing, just distribute the pots
				# to everyone who isn't folded
				@seating.forEachFrom @smallBlind, (player) =>
					if @isActive player
						winners.push player

			else
				# how many active players are there in this pot?
				activePlayersInPot = []

				for name of pot.bets
					player = @seating.findByName name
					if not player
						console.log 'Player in this pot already left game...'
					else if @isActive player
						activePlayersInPot.push player

				console.log "active players in pot: #{activePlayersInPot}"

				# if there's only one active, no need to eval hands
				if activePlayersInPot.length is 1
					console.log "only one active player in pot"
					winners.push activePlayersInPot[0]
				else
					[winners, losers] = @evalHands pot

			@distributeWinnings pot, winners

			# for first pot, which everyone will be in, tally up wins/losses
			if i is 0
				for player in winners
					player.winGame()
					@io.sockets.in(@id).emit GameMessages.PLAYER_INFO,
						position: parseInt @seating.find player
						message: "WINNER"
						chips: player.chips
						wager: 0

				for player in losers
					# folded players already had their loss tallied
					if player.state isnt PlayerStates.FOLDED
						player.loseGame()

		@endGame()

	# determine the players who won the given pot
	evalHands: (pot) ->
		playerHandValues = {}

		console.log "Pot:"
		console.log pot

		for name in _.keys pot.bets
			# for each player, combine their hand with the community cards
			# and run it thru the hand evaluator
			console.log name
			player = @seating.findByName name

			continue if not player?

			if not @isActive player
				console.log 'Player has invalid state, skipping'
				console.log 'state: ' + player.state
				continue

			# show everyone the player's hand
			@io.sockets.in(@id).emit GameMessages.CARDS,
				card1: player.hand[0].toModel().toJSON()
				card2: player.hand[1].toModel().toJSON()
				position: parseInt @seating.find player

			# figure out the value of the player's hand combined with
			# community cards
			mergedHand = player.hand.concat @community
			mergedHandStr = (card.toString() for card in mergedHand).join ','

			console.log "merged hand: " + mergedHandStr

			rank = @handevaluator.Eval mergedHandStr

			playerHandValues[name] = rank

		# figure out smallest rank (strongest hand)
		for own name, rank of playerHandValues
			if not smallest?
				smallest = rank
			else if rank < smallest
				smallest = rank

		console.log 'playerHandValues:'
		console.log playerHandValues

		winners = []
		losers = []

		# now return all players who had the smallest rank
		for own name, rank of playerHandValues
			if rank is smallest
				winners.push @seating.findByName name
			else
				losers.push @seating.findByName name

		return [winners, losers]

	distributeWinnings: (pot, winners) ->
		# for each pot, distribute winnings to player(s) with lowest rank
		# in that pot
		# for pot in @pots
		console.log 'Pot has ' + pot.totalValue() + ' chips'

		eachShare = 0

		# we have a leftover if the pot's total value isn't evenly divided by
		# the number of winners
		leftover = pot.totalValue() % winners.length

		# prevent chips from being broken down beyond whole numbers
		if leftover > 0
			adjustedTotal = pot.totalValue() - leftover
			eachShare = adjustedTotal / winners.length
		else
			eachShare = pot.totalValue() / winners.length

		for player in winners
			console.log "Giving #{player.name} #{eachShare} chips"
			player.giveChips eachShare

		# give leftover chips to players to the left of the dealer
		@seating.forEachFrom @dealer, (player) ->
			for winner in winners
				if leftover == 0
					break

				if player.name is winner.name
					player.giveChips 1
					leftover--

	endGame: ->
		console.log 'game.endGame'
		endGameTimerCallback = =>
			@io.sockets.in(@id).emit GameMessages.GAME_END
			@endGameCallback()

		setTimeout endGameTimerCallback, Game::POST_GAME_TIME * 1000

	betting: ->
		# begin betting round:
		# if GameState is DRAW, start from the next person after the bigBlind; else start from dealerPosition
		console.log "*** determining betting position ***"

		# find betting start position
		if @seating.at(@smallBlind)?
			console.log "starting at smallBlind: " + @smallBlind
			@currentPosition = @smallBlind
		else
			@currentPosition = @seating.next @smallBlind
			console.log "had no one at smallBlind, starting at " + @currentPosition

		while @seating.at(@currentPosition).state isnt PlayerStates.PLAYING
			console.log "player at " + @currentPosition + " isn't playing, looking for next player"
			@currentPosition = @seating.next @currentPosition

		@bettingPlayer = @seating.at @currentPosition

		console.log "decided on player/position: " + @bettingPlayer.name + " " + @currentPosition

		console.log "*** starting betting round ***"

		activePlayers = []
		allinPlayers = []

		@seating.forEachFrom @smallBlind, (player) =>
			if @isActive player
				activePlayers.push player

			if player.state is PlayerStates.ALL_IN
				allinPlayers.push player

		# only ask player to place a bet if they are not the only one not all-in/folded
		# OR if they haven't placed the minimum required bet
		if (@bettingPlayer.state isnt PlayerStates.ALL_IN and
		(allinPlayers.length + 1) isnt activePlayers.length) or
		@bettingPlayer.wager < @minBet

			@bet.placeBet @bettingPlayer,
					position: @currentPosition
					wager: @bettingPlayer.wager
					minBet: @minBet
					minRaise: @getMinRaise(GameMessages.BET)
					maxRaise: @getMaxRaise(GameMessages.SMALL_BLIND, @bettingPlayer)
					increment: @baseBet
					actions: @getAllowedActions(@bettingPlayer, GameMessages.BET)
					callback: _.bind @onBet, @

		else @nextBet()

	cancelBetting: ->
		@bet.cancelBet()

	addBet: (player, chipsBet) ->
		chipsRemaining = chipsBet

		# go thru each pot and add as much as we can to it
		for pot, i in @pots
			console.log 'name: ' + player.name + 'chips: ' + chipsRemaining + 'pot #:' + i

			# do we have more chips than the pot allows?
			if pot.maxBet? and chipsRemaining > pot.maxBet
				chipsRemaining -= pot.maxBet
				pot.setBet player.name, pot.maxBet
				console.log 'maxed out pot with ' + pot.maxBet + ' chips'
			else
				pot.setBet player.name, chipsRemaining
				console.log 'put all remaining chips into pot'

				if player.state is PlayerStates.ALL_IN
					pot.maxBet = chipsRemaining
					@recountPots()
				break

	recountPots: ->
		# create new side pot
		@pots.push new Pot

		for pot, i in @pots
			for player, bet of pot.bets
				if not pot.maxBet?
					console.log 'Invalid recountPots call,
						must be called when all pots have a maxBet'
					return

				# if bet exceeds pot's max bet, push over into next pot
				if bet > pot.maxBet
					pot.setBet player, pot.maxBet
					leftOver = bet - pot.maxBet
					@pots[i + 1].setBet player, leftOver


	onBet: (player, action, chipsBet) ->
		# @TODO support creating side pot, check all-in and see if they have less chips than the minimum bet

		if @nextState is GameStates.WINNINGS
			console.log 'Already doing winnings, invalid onBet call!'
			return

		# If player went all in, mark him as such
		if action == GameMessages.ALL_IN
			player.allIn = true

		# handle player's action
		console.log "Game.onBet"
		console.log "\tplayer: " + player.name
		console.log "\taction: " + action
		console.log "\tchipsBet: " + chipsBet
		console.log '\tminBet: ' + @minBet
		console.log "\tbaseBet: " + @baseBet
		console.log '\tallIn?: ' + player.allIn
		console.log '\tchips: ' + player.chips

		minAdditionalChips = @minBet - player.wager

		if minAdditionalChips > player.chips
			minAdditionalChips = player.chips

		maxChips = @minBet + @getMaxRaise(action, player)

		# weed out some invalid actions first to not have to handle all of them later
		# if chipsBet > player.chips or (chipsBet < minAdditionalChips and action isnt GameMessages.SMALL_BLIND and action isnt GameMessages.FOLD and action isnt GameMessages.ALL_IN) or (player.wager + chipsBet) > maxChips
		if chipsBet > player.chips
			console.log "Player input failed logic test, folding. chipsBet #{chipsBet} player.chips #{player.chips}"
			action = GameMessages.FOLD

		switch action
			when GameMessages.CHECK
				chipsBet = 0

			when GameMessages.FOLD
				player.state = PlayerStates.FOLDED
				chipsBet = 0

			when GameMessages.SMALL_BLIND
				if chipsBet < @smallBlindBet
					console.log 'Player posted small blind but bet less than small blind, folding'
					action = GameMessages.FOLD
					player.state = PlayerStates.FOLDED
				else
					player.takeChips chipsBet
					player.wager = chipsBet
					@addBet player, player.wager
					console.log 'Added small blind to pot, pot is now ' + @pots[0].totalValue()

			when GameMessages.BIG_BLIND
				player.takeChips chipsBet
				player.wager = chipsBet
				@addBet player, player.wager
				console.log 'Added big blind to pot, pot is now ' + @pots[0].totalValue()

			when GameMessages.CALL
				if chipsBet isnt minAdditionalChips
					console.log "Invalid call action, chipsBet must be the same as minBet"
					action = GameMessages.FOLD
					player.state = PlayerStates.FOLDED
				else
					console.log 'Player called, adding their chips to pot'
					player.takeChips chipsBet
					player.wager += chipsBet
					@addBet player, player.wager
					console.log 'Pot is now ' + @pots[0].totalValue() + ' chips'

			when GameMessages.RAISE
				if chipsBet <= minAdditionalChips
					console.log "Invalid raise action, chipsBet cannot be less than minBet"
					console.log "chipsBet: #{chipsBet} minAdditionalChips: #{minAdditionalChips} latestRaise: #{@latestRaise} minRaise: #{@minRaise}"
					action = GameMessages.FOLD
					player.state = PlayerStates.FOLDED
				else
					console.log 'Raising min bet to ' + chipsBet
					@minBet += (chipsBet - minAdditionalChips)
					@latestRaise = chipsBet
					player.takeChips chipsBet
					player.wager += chipsBet
					@addBet player, player.wager
					player.raiseCount++
					@gameRaiseCount++

			when GameMessages.ALL_IN
					player.wager += player.chips
					player.takeChips player.chips
					player.state = PlayerStates.ALL_IN

					@addBet player, player.wager

					if player.wager > @minBet
						@minBet = player.wager

		if action is GameMessages.FOLD
			displayChips = 0
			player.loseGame()
		else
			displayChips = player.wager

		# let everyone else know player's action
		@io.sockets.in(@id).emit GameMessages.BETTING,
			name: player.name
			position: @currentPosition
			action: action
			bet: displayChips
			chipsLeft: player.chips

		@io.sockets.in(@id).emit GameMessages.GAME_INFO,
			mainPot: @pots[0].totalValue()

		# go to next bet
		@nextBet()

	nextBet: ->
		activePlayers = []
		allinPlayers = []

		console.log 'Game.nextBet'

		@seating.forEachFrom @smallBlind, (player) =>
			console.log "#{player.name}: #{player.state}"
			if @isActive player
				activePlayers.push player

			if player.state is PlayerStates.ALL_IN
				allinPlayers.push player

		console.log '\tactivePlayers == ' + activePlayers.length
		console.log '\tallinPlayers == ' + allinPlayers.length

		if activePlayers.length is 0
			return

		if activePlayers.length is 1
			@showdown()
			return

		# console.log "Seating:"
		# for own pos, player of @seating.map
		# 	console.log pos + " -- " + player.name

		# in pre-draw, end after big blind
		if @nextState is GameStates.DRAW
			endPosition = @seating.next @bigBlind
		else
			endPosition = @smallBlind

		@currentPosition = @seating.next @currentPosition
		@bettingPlayer = @seating.at @currentPosition

		console.log '\tcurrent position == ' + @currentPosition
		console.log '\tBetting player: ' + @bettingPlayer.name

		needAnotherBetRound = false

		# check everyone's wagers for equality to see if we need to do another betting round
		if @nextState isnt GameStates.DRAW
			console.log 'Checking wagers...'
			console.log 'minBet == ' + @minBet

			@seating.forEachFrom @smallBlind, (player) =>
				return if player.state isnt PlayerStates.PLAYING

				if player.wager < @minBet
					console.log player.name + ' has a wager of ' + player.wager
					needAnotherBetRound = true

		# during pre-draw, betting round is only small blind and big blind
		if @nextState is GameStates.DRAW and @currentPosition is @bigBlind
			console.log '\tSending bet to big blind'
			console.log "\n sent minRaise of: #{@getMinRaise(GameMessages.BET)}"
			@bet.placeBet @bettingPlayer,
					position: @currentPosition
					wager: @bettingPlayer.wager
					minBet: @baseBet
					minRaise: @getMinRaise(GameMessages.BET)
					maxRaise: @getMaxRaise(GameMessages.BIG_BLIND, @bettingPlayer),
					increment: @baseBet
					actions: @getAllowedActions(@bettingPlayer, GameMessages.BIG_BLIND)
					callback: _.bind @onBet, @
		else if @currentPosition isnt endPosition
			# skip all in players and folded
			if @bettingPlayer.state isnt PlayerStates.PLAYING
				console.log @bettingPlayer.name
				console.log "Current player isn't playing, going to next player"
				@nextBet()
			else if @bettingPlayer.state is PlayerStates.ALL_IN
				console.log "#{@bettingPlayer.name} is all-in, skipping them"
				@nextBet()
			else
				# if all the other players are all-in, the remaining player automatically
				# "checks" if they have >= to the minimum amount
				if activePlayers.length is (allinPlayers.length + 1) and
				@bettingPlayer.wager >= @minBet

					console.log @bettingPlayer.name + ' only non-allin player left'
					console.log 'wager: ' + @bettingPlayer.wager + ' minBet: ' + @minBet
					@advanceState()

				else
					console.log "Putting bet down"
					@bet.placeBet @bettingPlayer,
							position: @currentPosition
							wager: @bettingPlayer.wager
							minBet: @minBet
							minRaise: @getMinRaise(GameMessages.BET)
							maxRaise: @getMaxRaise(GameMessages.BET, @bettingPlayer)
							increment: @baseBet
							actions: @getAllowedActions(@bettingPlayer, GameMessages.BET)
							callback: _.bind @onBet, @
		else if needAnotherBetRound
			console.log "Not everyone's wagers were the same, starting another round of betting"
			@betting()
		else
			console.log "Got to endPosition"
			# if all bets are over, go to next round
			@advanceState()

	onUserJoin: (user) ->
		# update user with current state of the game
		console.log 'Game.onUserJoin'

		# send main pot info
		@io.sockets.in(@id).emit GameMessages.GAME_INFO,
			mainPot: @pots[0].totalValue()
			dealer: @dealer

		# for each currently playing player, send their info
		@seating.forEachFrom 1, (player, pos) =>
			user.emit GameMessages.PLAYER_INFO,
				position: pos
				chips: player.chips
				bet: player.wager

		# if we have any community cards out, send them
		if @community.length > 0
			@sendCards GameMessages.GAME_INFO, user

	isActive: (player) ->
		return player.state isnt PlayerStates.FOLDED and
			player.state isnt PlayerStates.OBSERVING and
			player.state isnt PlayerStates.BOUGHT_IN

	raisePossible: ->
		console.log("called raisePossible")
		activePlayers = []
		allinPlayers = []
		@seating.forEachFrom @smallBlind, (player) =>
			if @isActive player
				activePlayers.push player

			if player.state is PlayerStates.ALL_IN
				allinPlayers.push player

		console.log("\t active: #{activePlayers.length}")
		console.log("\t #{allinPlayers.length}")

		return ((activePlayers.length - allinPlayers.length) > 1) ? true : false

exports.PlayerStates = PlayerStates
exports.GameStates = GameStates
exports.Game = Game
