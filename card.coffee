class Card
	SUITS = "cdhs"
	FACES = "L23456789TJQKA"
	SUIT_LOOKUP = {
		c: 0,
		d: 1,
		h: 2,
		s: 3
	}

	FACE_VALUES = {
		'L': 1,
		'2': 2,
		'3': 3,
		'4': 4,
		'5': 5,
		'6': 6,
		'7': 7,
		'8': 8,
		'9': 9,
		'T': 10,
		'J': 11,
		'Q': 12,
		'K': 13,
		'A': 14
	}

	constructor: (sym) ->
		@card = sym
		@suit = sym[0]
		@face = sym[1]
		@suit.toLowerCase()
		@face = Card.face_value @face
		@suit = SUIT_LOOKUP[@suit]
		@value = (@suit * FACES.length) + (@face - 1)

	eql: (card) ->
		@value == card.value
	
	gt: (card) ->
		@value > card.value
	
	lt: (card) ->
		@value < card.value

	Card.face_value = (face) ->
		face = face.toUpperCase()
		if face == 'L' || !FACE_VALUES.hasOwnProperty? face
			null
		else
			FACE_VALUES[face]
