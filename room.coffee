# room has a table which shows the seated players and lets people who joined the room
# pick a spot at a table

Table = require './table'
GameMessages = require './game_io'
message = require './message'
_ = require 'underscore'

class Room
	constructor: (@id, @io, type) ->
		@table = new Table @id, @io, type
		@users = {}

	join: (user) ->
		# when user joins room, subscribe them to room id
		user.join @id

		# console.log 'user ' + user.username + ' just entered room ' + @id

		# add them to list of people in room
		@users[user.mongoId] = {}

		# let everyone in the room know a user joined
		@io.sockets.in(@id).emit GameMessages.ROOM_JOIN, message: user.username + ' je upravo ušao u sobu.'
		# delegate user join event to table object
		@table.onUserJoin user

		# listen for user's sit down message
		message.listen
			io: user
			message: GameMessages.TABLE_SIT
			onMessage: _.bind @onUserSit, @
			requiredParams: ['position']

		# for now, the only way a user can leave a room is by disconnecting the socket, meaning
		# the user has navigated away from the page
		user.on 'disconnect', => @onUserDisconnect user

	onUserSit: (data, user, removeListen) ->
		# console.log 'Room.onUserSit'
		position = data.position

		success = @table.seatUser user, position

		user.emit GameMessages.TABLE_SIT_RESPONSE, pos: position, result: success

		# console.log @users

		# if they chose an acceptable spot, sit them down, stop listening
		# for accept, and let everyone know the user sat down
		if success
			# console.log "********** FROM /lib/room.coffee ************"
			# console.log "#{user.username} sat at position #{position}"
			@io.sockets.in(@id).emit GameMessages.TABLE_SIT, 
				name: user.username
				position: position

			removeListen()

			# listen for user's get up message
			message.listen
				io: user
				message: GameMessages.TABLE_GETUP
				onMessage: _.bind @onUserGetUp, @

	onUserGetUp: (user, removeListen) ->
		# stop listening for get up message
		removeListen()
		
		# listen for user's sit down message
		message.listen
			io: user
			message: GameMessages.TABLE_SIT
			onMessage: _.bind @onUserSit, @
			requiredParams: ['position']

	onUserDisconnect: (user) ->
		@io.sockets.in(@id).emit GameMessages.ROOM_LEAVE, message: user.username + ' je izašao iz sobe'

		# remove them from list of users in room
		delete @users[user.mongoId]

	leave: (user) ->
		user.io.leave @id

module.exports = Room
