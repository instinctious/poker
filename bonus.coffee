# bonus awards module

exports = module.exports

BONUS_CHIPS = 50
BONUS_DAY_FREQUENCY = 5

LOG_LEVEL = 1

needsBonus = (lastBonusDate) ->
	currentDate = new Date()
	nextBonusDate = new Date(lastBonusDate)
	nextBonusDate.setDate(nextBonusDate.getDate() + BONUS_DAY_FREQUENCY)

	if LOG_LEVEL > 0
		console.log "current date: #{currentDate}"
		console.log "last bonus date: #{lastBonusDate}"
		console.log "date for next bonus: #{nextBonusDate}"

	return currentDate >= nextBonusDate

giveBonus = (mongooseDoc) ->
	if LOG_LEVEL > 0
		console.log "giving #{mongooseDoc.email} #{BONUS_CHIPS} chips"
	mongooseDoc.chips += BONUS_CHIPS

	# update last bonus date to now
	mongooseDoc.lastChipBonus = new Date()
	mongooseDoc.save()

exports.needsBonus = needsBonus
exports.giveBonus = giveBonus