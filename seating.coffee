game = require './game'

# this class represents the seating arrangement at a table
# seats are allowed to be empty, so at a table of size 5
# you could have players sitting at seats 0, 2, and 4
class Seating
	constructor: (@tableSize) ->
		# depiction of a table's seating, with key = seat number, value = user
		@map = {}

	# returns the seating position of the user, or -1 if not found
	find: (user) ->
		for own position of @map
			if @map[position].name == user.name
				return parseInt position
		
		return -1

	findByName: (name) ->
		for own position of @map
			if @map[position].name == name
				return @map[position]
		
		return null
	
	# attempts to add a user at position, returns false if the position's already taken or if the position is invalid
	seat: (user, position) ->
		if 1 <= position <= @tableSize and not @map[position]?
			# console.log "*** FROM seating.coffee ***"
			# console.log "#{user.name} successfully added on the position #{position}"
			# console.log "**********************"
			@map[position] = user
			return true
		else
			# console.log "*** FROM seating.coffee ***"
			# console.log "Sorry, user: "
			# console.log user
			# console.log " cannot sit at the position #{position} because it is already taken by someone else."
			# console.log "**********************"
			return false

	remove: (position) ->
		delete @map[position] if @map.hasOwnProperty position

	# call function for each member of table, starting from indicated member
	forEachFrom: (fromPosition, func) ->

		# we have to loop twice, once to do fromPosition to end of map
		# and another time to go from beginning of map to just before fromPosition

		for own position of @map when position >= fromPosition
			func @map[position], parseInt position

		for own position of @map when position < fromPosition
			func @map[position], parseInt position

	# returns the position of the first seated player at the table
	first: ->
		for own position of @map
			return parseInt position
		
		# if we got here, we have no people sitting at table
		return -1

	# returns the position of the player sitting at the table next clockwise after the given position
	next: (afterPosition) ->
		for own position of @map when afterPosition < position
			return parseInt position

		# we reached the end of the map
		return parseInt @.first()

	# same as next(), but skips players that are not active in the current game
	nextActive: (afterPosition) ->
		pos = @.next afterPosition
		user = @.at pos

		while user.state isnt game.PlayerStates.PLAYING and
		user.state isnt game.PlayerStates.ALL_IN
			pos = @.next pos
			user = @.at pos

		return pos

	# returns the user at given position
	at: (position) ->
		return @map[position]

	count: ->
		count = 0
		count++ for own pos of @map
		return count

exports = module.exports = Seating
