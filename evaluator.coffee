handevaluator = require './build/Release/handevaluator'

evaluator = new handevaluator.HandEval

loaded = false

module.exports =
	load: (callback) ->
		evaluator.Load ->
			loaded = true
			callback()

	get: ->
		if not loaded
			throw new Error("Hand evaluator must be loaded before it can be used")

		return evaluator
