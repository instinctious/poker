sanitizer = require 'sanitizer'
_ = require 'underscore'

exports = module.exports

# Replace all non alphanumeric chars with nil and spaces with dash
stripTags = (msg) ->
	regex = new RegExp /[^a-zA-Z0-9\s]/ig
	msg = msg.replace(regex, '')
	msg = msg.replace(' ', '-')
	return msg

logLevel = 0

listen = (response) ->
	if response.timeout and not response.singleShot
		console.log "Invalid send call, can't have timeout and not be single shot"
		return

	onMessage = (data) ->
		if logLevel > 0
			console.log 'send.onMessage'
			console.log data

		# validate that data contains all of the response's params
		if response.requiredParams?
			for param in response.requiredParams
				if not data[param]?
					console.log "Error for message " + message
					console.log param + " was missing from client response"
					return

				if _.isString data[param]
					data[param] = stripTags data[param]

		if response.singleShot
			clearListeners()

		if response.requiredParams?
			response.onMessage data, response.io, clearListeners, 
				response.additionalParams
		else
			response.onMessage response.io, clearListeners, response.additionalParams

	onTimeout = ->
		clearListeners()
		response.onTimeout response.io, response.additionalParams

	onDisconnect = ->
		clearListeners()
		if response.onDisconnect
			response.onDisconnect response.io, response.additionalParams

	clearListeners = ->
		if timeout
			if logLevel > 0
				console.log 'clearing timeout with id ' + timeout.id
			clearTimeout timeout
		response.io.removeListener response.message, onMessage
		response.io.removeListener 'disconnect', onDisconnect

	response.io.on response.message, onMessage
	response.io.on 'disconnect', onDisconnect
	if response.timeout
		timeout = setTimeout response.onTimeout, response.timeout * 1000
		timeout.id = response.io.username
		if logLevel > 0
			console.log 'set timeout id to ' + timeout.id

	return clearListeners	

send = (io, message, params, response) ->

	if logLevel > 0
		console.log 'send: ' + message
		console.log params

	io.emit message, params

	return if not response?

	listen response

exports.send = send
exports.listen = listen
exports.logLevel = logLevel

