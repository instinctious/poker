game = require './game'

class Player
	constructor: (@user, @chips = 0, @state = game.PlayerStates.PLAYING) ->
		@name = @user.username
		@wager = 0
		@hand = []
		if @chips > 0
			@buyInChips @chips
		# To track whether player went all in already
		@allIn = false

		@wins = 0
		@losses = 0

		@raiseCount = 0

	# takes from player's main chip pile and adds to their game chip pile
	buyInChips: (chips) ->
		@chips = chips
		@user.doc.chips -= chips
		@user.doc.save (err) =>
			if err
				console.log 'Player.constructor'
				console.log 'Error while saving #{@name} to database!'
				console.log err		

	# returns player's game chips back to their main chip pile
	cashOutChips: ->
		@user.doc.chips += @chips
		@user.doc.wins += @wins
		@user.doc.losses += @losses
		@chips = 0
		@user.doc.save (err) =>
			if err
				console.log 'Player.returnChips'
				console.log 'Error while saving #{@name} to database!'
				console.log err		
	
	giveChips: (chips) ->
		@chips += chips

	winGame: (wonChips) ->
		@wins++

	loseGame: ->
		@losses++

	takeChips: (chips) ->
		if chips > @chips
			console.log "Player.takeChips LOGIC ERROR"
			console.log "\t#{@name} has #{@chips} chips, but we're taking #{chips} chips from them"
		@chips -= chips
		console.log "#{@name} now has #{@chips} chips"

exports = module.exports = Player
